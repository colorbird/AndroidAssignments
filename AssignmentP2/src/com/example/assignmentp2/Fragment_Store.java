package com.example.assignmentp2;

import com.example.assignmentp2.Fragment_Two.OnDoneButtonClickedListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class Fragment_Store extends DialogFragment  {
	
		private EditText mEditText;
		private Button  btStoreCan;
		private Button  btStoreOk;
		EditNameDialogListener mCallback1;
		
		public interface EditNameDialogListener {
	        void onFinishEditDialog(String inputText);
	    }
		
		public Fragment_Store(){}
		
		public static Fragment_Store newInstance(String title) {
		    Fragment_Store frag = new Fragment_Store();
		    Bundle args = new Bundle();
		    args.putString("title", title);
		    frag.setArguments(args);
		    return frag;
		   }
		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	        View view = inflater.inflate(R.layout.fragment_store, container,false);
	        mEditText = (EditText) view.findViewById(R.id.txt_your_name);
	        btStoreCan= (Button)   view.findViewById(R.id.btnStoreCancel);
	        btStoreOk=  (Button)   view.findViewById(R.id.btnStoreOk);
	        
	        getDialog().setTitle("Store");
            
	        //Show soft keyboard automatically
	        /*mEditText.requestFocus();
	        getDialog().getWindow().setSoftInputMode(
	               LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	        mEditText.setOnEditorActionListener(this);*/

	        btStoreOk.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mCallback1.onFinishEditDialog(mEditText.getText().toString());
				}
			});
	        
	        btStoreCan.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
				 
					mCallback1.onFinishEditDialog("Cancel and go back");
					
					
				}
			});
	        return view;
	    }
	
		@Override
		 public void onAttach(Activity activity) {
		    super.onAttach(activity);
		       
		    try {
		         mCallback1 =  (EditNameDialogListener) activity;
		    	} catch (ClassCastException e) {
		    	throw new ClassCastException(activity.toString()
		          + " must implement OnHeadlineSelectedListener");
		    	}
		    }

	
	
   
}
