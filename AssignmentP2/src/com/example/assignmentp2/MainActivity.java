package com.example.assignmentp2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements
		Fragment_First.OnButtonClickedListener,
		Fragment_Two.OnDoneButtonClickedListener,
		Fragment_Store.EditNameDialogListener,
		Fragment_Load.OnLoadClickedListener {

	Fragment_First firstFragment;
	Fragment_Two secondFragment;
	Fragment_Store storeFragment;
	Fragment_View viewFragment;
	Fragment_Load loadFragment;
	FragmentTransaction transaction;
	ArrayList<String> saveFileData = new ArrayList<String>();

	ArrayList<String> nameOfAllFiles = new ArrayList<String>();
	String str;
	String loadedFilename = ""; // Filename selected by load button
	static final String ListofFileNames = "ListAllFiles";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ArrayList<String> allFiles = fileOpen(ListofFileNames);
		if (!allFiles.isEmpty()) {
			nameOfAllFiles = allFiles;
		}

		if (findViewById(R.id.LinearLayout123) != null) {

			if (savedInstanceState != null) {

				return;
			}
			// intent part not used yet
			firstFragment = new Fragment_First();
			makeTransactions(firstFragment);

		}
	}

	@Override
	public void onButtonSelected(String buttonName) {

		secondFragment = new Fragment_Two();
		storeFragment = new Fragment_Store();
		viewFragment = new Fragment_View();
		loadFragment = new Fragment_Load();
		if (buttonName.equalsIgnoreCase("btEnterName")) {
			transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.LinearLayout123, secondFragment);
			transaction.addToBackStack(null);
			transaction.commit();
		} else if (buttonName.equalsIgnoreCase("btStore")) {

			FragmentManager fm = getSupportFragmentManager();
			storeFragment.show(fm, "fragment_store");

		} else if (buttonName.equalsIgnoreCase("btLoad")) {

			// Passing the data to view element
			Bundle b = new Bundle();

			b.putStringArrayList("loadData", nameOfAllFiles);
			loadFragment.setArguments(b);
			makeTransactions(loadFragment);

		} else if (buttonName.equalsIgnoreCase("btView")) {
			if (loadedFilename != "") {

				makeBundle(fileOpen(loadedFilename));
				makeTransactions(viewFragment);
			} else {
				Toast toast = Toast.makeText(this, "No file loaded", 3);
				toast.show();
			}

		} else if (buttonName.equalsIgnoreCase("btExit")) {
			
			
			new AlertDialog.Builder(this)
            .setMessage("Are you sure you want to save and  quit?")
            .setCancelable(true)
            .setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                int id) {
                            finish();
                        }
                    }).setNegativeButton("No", null).show();

		}

	}
	

	// function for moving between the fragments
	public void makeTransactions(Fragment sampleFragment) {
		transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.LinearLayout123, sampleFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	// Done Button Functionality after clicking Enter names
	@Override
	public void onDoneButtonSelected(ArrayList<String> customerData) {

		for (int i = 0; i < customerData.size(); i++) {
			saveFileData.add(customerData.get(i));
		}

		transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.LinearLayout123, firstFragment);
		transaction.addToBackStack(null);
		transaction.commit();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// Store Button saving functionality
	@Override
	public void onFinishEditDialog(String inputText) {
		if (inputText.equalsIgnoreCase("Cancel and go back")) {
			transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.LinearLayout123, firstFragment);
			// transaction.addToBackStack(null);
			transaction.commit();
		} else if ((!inputText.equals("")) && (!inputText.equals(null))) {
			Toast.makeText(this,   ""+ inputText +" file Stored", Toast.LENGTH_SHORT).show();

			transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.LinearLayout123, firstFragment);
			// transaction.addToBackStack(null);
			transaction.commit();

			// adding the file names in an arrayList
			nameOfAllFiles.add(inputText);

			saveToFile(saveFileData, inputText);
			saveFileData.clear();
			saveToFile(nameOfAllFiles, ListofFileNames);
		}

	}

	public void saveToFile(ArrayList<String> arrayName, String FileName) {

		str = "";
		if (!FileName.equalsIgnoreCase(ListofFileNames)) {
			for (int i = 0; i < arrayName.size(); i++) {
				
				if ((i + 1) % 3 == 0) {
					str = str + arrayName.get(i) + "\n";
				} else {
					str = str + arrayName.get(i) + "                ";
				}

			}
		} else {
			for (int i = 0; i < arrayName.size(); i++)
				str = str + arrayName.get(i) + "\n";
		}

		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					openFileOutput(FileName, Context.MODE_PRIVATE));
			outputStreamWriter.write(str);
			outputStreamWriter.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			InputStream inputStream = openFileInput(FileName);
			inputStream.close();
		}

		catch (FileNotFoundException e) {

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private ArrayList<String> fileOpen(String fileName) {
		ArrayList<String> viewFileData = new ArrayList<String>();
		try {
			InputStream IS = openFileInput(fileName);
			InputStreamReader ISR = new InputStreamReader(IS);
			BufferedReader BR = new BufferedReader(ISR);
			String str;

			while ((str = BR.readLine()) != null) {
				viewFileData.add(str);
			}

			IS.close();

		} catch (FileNotFoundException e) {
			 e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return viewFileData;
	}

	private void makeBundle(ArrayList<String> sendFileData) {
		// Passing the data to view element
		Bundle b = new Bundle();
		b.putStringArrayList("alldata", sendFileData);
		viewFragment.setArguments(b);
	}

	@Override
	public void onLoadSelected(String loadName) {
		loadedFilename = loadName;

	}

}
