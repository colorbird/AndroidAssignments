package com.example.assignmentp4;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;


public class SectionsPagerAdapter extends FragmentPagerAdapter {

	
	
	public SectionsPagerAdapter(FragmentManager fm) {
		super(fm);
	}
String[] array1;
	public void SetArguements(String[] string)
	{
		array1 =string;
	}

	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		Fragment fragment = new DummyFragment();
		Bundle args = new Bundle();
		args.putInt(DummyFragment.ARG_SECTION_NUMBER, position + 1);
		args.putStringArray("key", array1);
		args.putInt("pos", position);
		Log.d("arrayposition",array1[position]);
		args.putString("name", array1[position]);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public int getCount() {
//	return the number of cursor.gecount	
		return array1.length;
	}

}