package com.example.assignmentp4;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;






import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	 Button btPopulate;
	 Button btSearch;
	 WebView browser;
	 String HTML="";
	 private TextView textView;
	 EditText editText;
	 public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	 private ProgressDialog mProgressDialog;
	 DownloadWebPageTask task;
	 private String textUrl="";
	 private DatabaseHelper db=null;
	 String[] names={""};
	 loadWebSearch loadSearch;
	protected Context context;
	 
	 @Override
	    protected Dialog onCreateDialog(int id) {
	        switch (id) {
	        case DIALOG_DOWNLOAD_PROGRESS:
	            mProgressDialog = new ProgressDialog(this);
	            mProgressDialog.setMessage("Downloading Names");
	            mProgressDialog.setCancelable(false);
	            mProgressDialog.show();
	            return mProgressDialog;
	        default:
	        return null;
	        }
	    }
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//textView = (TextView) findViewById(R.id.TextView01); 
		//browser = (WebView) findViewById(R.id.webview);
		editText= (EditText)  findViewById(R.id.editText1);
		btSearch=(Button) findViewById(R.id.btSearch);
		btPopulate = (Button) findViewById(R.id.btPopulate);
		btSearch.setEnabled(false);
		int position=editText.getText().toString().length();
		editText.setSelection(position);
		
		db= new DatabaseHelper(getApplicationContext());
        btPopulate.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                	task = new DownloadWebPageTask();
                	
                	
                	db.getWritableDatabase().delete(DatabaseHelper.TABLE, null, null);
                	//textUrl="http://www.eecg.utoronto.ca/~jayar/PeopleList";
                    task.execute();	
                    
                  
                }
            }
        );
       btSearch.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			loadSearch=new loadWebSearch();
			loadSearch.execute();
			
			
		}
	});
    }
	
	@Override
	  public void onDestroy() {
	    super.onDestroy();

	    db.close();
	  }
	
	
	
private class DownloadWebPageTask extends AsyncTask<Void, String, String> {
    	
	private Cursor constantsCursor=null;

    	@SuppressWarnings("deprecation")
		@Override
        protected void onPreExecute() {
            super.onPreExecute();
            textUrl=editText.getText().toString();
            if(textUrl!=null){
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
            }
            else
            {
    			Toast.makeText(getApplicationContext(), "Enter proper Url", Toast.LENGTH_SHORT).show();
            }
            
        }
    	
    	@Override
        protected String doInBackground(Void... params) {
            String response = "";
            
            String[] urls= {textUrl};
            if(textUrl!=null)
            {
            for (String url : urls) {
            	   try {
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
             
                    HttpResponse execute = client.execute(httpGet);
                    InputStream content = execute.getEntity().getContent();

                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(content));
                    String s = "";
                    //logic for db
                    ContentValues cv=new ContentValues();
                    
                    db.onCreate(db.getWritableDatabase());
                    while ((s =  buffer.readLine()) != null) {
                        response = response + s ;
                        //logic for db
                        cv.put(DatabaseHelper.TITLE,s);
                        
                      db.getWritableDatabase().insert(DatabaseHelper.TABLE, DatabaseHelper.TITLE,cv);  
                        
                    }
                    
                 

                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                		
            }	
            }
            
            return response;
        }
    	protected void onProgressUpdate(String... progress) {    
    		
    	    mProgressDialog.setProgress(Integer.parseInt(progress[0]));
    	}
    		
    	
        @SuppressWarnings("deprecation")
		@Override
        
        protected void onPostExecute(String result) {
        	dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
           
        	btSearch.setEnabled(true);
            btSearch.setTextColor(Color.BLACK);
         
        }
        
       
    }


private class loadWebSearch extends AsyncTask<Void, Void, Void> {
    
	private Cursor constantsCursor1=null;
	Bundle bundle;
	
	@Override
	protected Void doInBackground(Void... params) {
		
		constantsCursor1=db.getReadableDatabase().rawQuery("SELECT _id, title "
                        + "FROM constants1",
                        null);
                constantsCursor1.getCount();
		return null;
	}
	
	protected void onPostExecute(Void notUsed){
		if(constantsCursor1.getCount()!=0)
		{
	    constantsCursor1.moveToFirst();
  	    names = new String[constantsCursor1.getCount()];
  		int iterator=0;
  		names [iterator]=constantsCursor1.getString(constantsCursor1.getColumnIndex("title"));
  		while(constantsCursor1.moveToNext())
  		{
  			iterator++;
  			names[iterator]=constantsCursor1.getString(constantsCursor1.getColumnIndex("title"));
  			//Log.d("ValueofNames", names[iterator]);
  			
  		}
  		bundle = new Bundle();
  		bundle.putStringArray("key",names);
  		
  		
  		Intent intent = new Intent(getApplicationContext(),MultipleTabs.class);
  		intent.putExtras(bundle);
  		startActivity(intent);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Enter proper Url", Toast.LENGTH_SHORT).show();
		}
	}
	
}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
